package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
