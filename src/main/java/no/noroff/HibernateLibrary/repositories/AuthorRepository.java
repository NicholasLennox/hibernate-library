package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    Boolean existsByFirstNameAndLastNameAndDob(String firstName, String lastName, Date dob);
    Author findByFirstNameOrDobAfterOrderByLastNameDesc(String firstName, Date dob);
}

