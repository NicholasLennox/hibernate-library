package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Library;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryRepository extends JpaRepository<Library,Long> {
}
