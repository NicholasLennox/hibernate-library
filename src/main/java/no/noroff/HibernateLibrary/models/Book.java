package no.noroff.HibernateLibrary.models;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;


    @NaturalId
    @Column(name = "isbn_10")
    private String isbn10;
    @NaturalId
    @Column(name = "isbn_13")
    private String isbn13;

    private String edition;
    @Column(name = "publish_date")
    private Date publishDate;

    @ManyToOne
    @JoinColumn(name = "author_id")
    public Author author;

    @JsonGetter("author")
    public String author() {
        if(author != null){
            return "/api/v1/authors/" + author.getId();
        }else{
            return null;
        }
    }
    // Book entity
    @ManyToMany(mappedBy = "books")
    public Set<Library> libraries;

    @JsonGetter("libraries")
    public List<String> librariesGetter() {
        if(libraries != null){
            return libraries.stream()
                    .map(library -> {
                        return "/api/v1/libraries/" + library.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
    // Getters and Setters

    public Long getId() {
        return id;
    }

}
