package no.noroff.HibernateLibrary.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number;
    private String street;
    private String city;
    @Column(name = "postal_code")
    private String postalCode;

    @JsonIgnore
    @OneToOne(mappedBy = "address")
    private Library library;

    // Getters and setters

    public Long getId() {
        return id;
    }


}
