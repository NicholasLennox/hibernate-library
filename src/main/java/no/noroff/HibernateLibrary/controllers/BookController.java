package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Author;
import no.noroff.HibernateLibrary.models.Book;
import no.noroff.HibernateLibrary.repositories.AuthorRepository;
import no.noroff.HibernateLibrary.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/books")
public class BookController {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping()
    public ResponseEntity<List<Book>> getAllBooks(){
        List<Book> books = bookRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(books,status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBook(@PathVariable Long id){
        Book returnBook = new Book();
        HttpStatus status;
        // We first check if the Book exists, this saves some computing time.
        if(bookRepository.existsById(id)){
            status = HttpStatus.OK;
            returnBook = bookRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnBook, status);
    }

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody Book book){
        HttpStatus status;
        book = bookRepository.save(book);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(book, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Book> updateAuthor(@PathVariable Long id, @RequestBody Book book){
        Book returnBook = new Book();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if(!id.equals(book.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnBook,status);
        }
        returnBook = bookRepository.save(book);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnBook, status);
    }
}
