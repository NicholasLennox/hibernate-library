package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Address;
import no.noroff.HibernateLibrary.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/addresses")
public class AddressController {
    @Autowired
    private AddressRepository addressRepository;

    @GetMapping
    public ResponseEntity<List<Address>> getAllAddresses(){
        List<Address> data = addressRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Address> getSpecificAddress(@PathVariable Long id){
        HttpStatus status;
        Address add = new Address();
        if(!addressRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(add, status);
        }
        add = addressRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(add, status);
    }

    @PostMapping
    public ResponseEntity<Address> addAddress(@RequestBody Address address){
        Address add = addressRepository.save(address);
        HttpStatus status = HttpStatus.CREATED;
        // Return a location -> url to get the new resource
        return new ResponseEntity<>(add, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Address> updateAddress(@RequestBody Address address, @PathVariable Long id){
        HttpStatus status;
        Address retAdd = new Address();
        if(!Objects.equals(id, address.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(retAdd, status);
        }
        retAdd = addressRepository.save(address);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(retAdd, status);
    }

}
