package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Author;
import no.noroff.HibernateLibrary.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 This controller uses a simple ResponseEntity composed with a specific entity model
 There is no error handling done, errors here will demonstrate how Spring handles REST API
 related errors. Hint, they use a common response similar to what we saw in the lecture.
 We use a HttpStatus object to handle the response code, we pass this with the ResponseEntity.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/authors")
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping()
    public ResponseEntity<List<Author>> getAllAuthors(){
        List<Author> authors = authorRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(authors,status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Author> getAuthor(@PathVariable Long id){
        Author returnAuth = new Author();
        HttpStatus status;
        // We first check if the author exists, this saves some computing time.
        if(authorRepository.existsById(id)){
            status = HttpStatus.OK;
            returnAuth = authorRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnAuth, status);
    }

    @PostMapping
    public ResponseEntity<Author> addAuthor(@RequestBody Author author){
        Author returnAuthor = authorRepository.save(author);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnAuthor, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Author> updateAuthor(@PathVariable Long id, @RequestBody Author author){
        Author returnAuthor = new Author();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if(!id.equals(author.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnAuthor,status);
        }
        returnAuthor = authorRepository.save(author);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnAuthor, status);
    }

}
