package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Author;
import no.noroff.HibernateLibrary.models.Library;
import no.noroff.HibernateLibrary.repositories.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/libraries")
public class LibraryController {

    @Autowired
    private LibraryRepository libraryRepository;

    @GetMapping()
    public ResponseEntity<List<Library>> getAllLibraries(){
        List<Library> libraries = libraryRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(libraries,status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Library> getLibrary(@PathVariable Long id){
        Library returnLib = new Library();
        HttpStatus status;
        // We first check if the Library exists, this saves some computing time.
        if(libraryRepository.existsById(id)){
            status = HttpStatus.OK;
            returnLib = libraryRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnLib, status);
    }

    @PostMapping
    public ResponseEntity<Library> addLibrary(@RequestBody Library library){
        
        Library returnLib = libraryRepository.save(library);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnLib, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Library> updateLibrary(@PathVariable Long id, @RequestBody Library library){
        Library returnLib = new Library();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if(!id.equals(library.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnLib,status);
        }
        returnLib = libraryRepository.save(library);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnLib, status);
    }
}
