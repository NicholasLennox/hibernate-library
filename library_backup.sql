--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

-- Started on 2020-10-30 10:32:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3023 (class 0 OID 16814)
-- Dependencies: 202
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.address (id, city, number, postal_code, street) VALUES (1, 'Oslo', '1', '0000', NULL);
INSERT INTO public.address (id, city, number, postal_code, street) VALUES (2, 'Oslo', '1', '0000', 'Main');
INSERT INTO public.address (id, city, number, postal_code, street) VALUES (3, 'Oslo', '2', NULL, 'Main');
INSERT INTO public.address (id, city, number, postal_code, street) VALUES (4, 'Oslo', '2', '0001', 'Main');
INSERT INTO public.address (id, city, number, postal_code, street) VALUES (5, 'Bergen', '20', '2525', 'Mayor');


--
-- TOC entry 3025 (class 0 OID 16824)
-- Dependencies: 204
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (1, '1992-08-11 02:00:00', 'Nicholas', 'Lennox');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (2, '1953-02-19 01:00:00', 'Dewald', 'Els');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (3, '2000-04-20 02:00:00', 'Craig', 'Marias');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (4, '1984-04-20 02:00:00', 'Erlend', 'Skar');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (15, '2020-09-29 02:00:00', 'Can we', 'Add?');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (16, NULL, 'Foo', 'Bar');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (17, NULL, 'foo2', 'baz2');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (19, NULL, 'Foo4', 'Bar4');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (20, NULL, 'foo', 'merge');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (21, NULL, 'New', 'author');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (22, NULL, 'New', 'Author 2');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (23, NULL, 'New', 'Author 3');
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (24, NULL, 'spicymeme', NULL);
INSERT INTO public.author (id, date_of_birth, first_name, last_name) VALUES (25, NULL, NULL, NULL);


--
-- TOC entry 3027 (class 0 OID 16834)
-- Dependencies: 206
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (1, 'Prime', '28378292', '323028248248024', '2020-01-07 01:00:00', 'Front end dev with cool hair', 2);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (2, 'Basic', '45346346', '235235235235255', '2020-09-03 02:00:00', 'Java for Beginners', 1);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (8, 'Standard', '23423432', '234255262626', '2016-06-25 02:00:00', 'Java for Experts', 3);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (9, 'Bosss', '75457457', '8848484848', '2018-02-25 01:00:00', 'Being a boss', 4);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (10, 'G6', '6658586585', '3463463646663', '2019-04-25 02:00:00', 'Owning a jet', 4);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (11, 'Beginner', '35757574', '4577555777', '2019-06-25 02:00:00', 'Learning to fly', 4);
INSERT INTO public.book (id, edition, isbn_10, isbn_13, publish_date, title, author_id) VALUES (12, 'W', '420420420', '3604206996024063', '2019-11-25 01:00:00', 'Flying high', 4);


--
-- TOC entry 3030 (class 0 OID 16847)
-- Dependencies: 209
-- Data for Name: library; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.library (id, name, address_id) VALUES (1, 'Noroff Oslo', NULL);
INSERT INTO public.library (id, name, address_id) VALUES (2, 'Noroff Bergen', NULL);
INSERT INTO public.library (id, name, address_id) VALUES (3, 'Noroff National', NULL);
INSERT INTO public.library (id, name, address_id) VALUES (4, 'Testing Library', NULL);


--
-- TOC entry 3028 (class 0 OID 16842)
-- Dependencies: 207
-- Data for Name: book_library; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.book_library (library_id, book_id) VALUES (1, 1);
INSERT INTO public.book_library (library_id, book_id) VALUES (1, 2);
INSERT INTO public.book_library (library_id, book_id) VALUES (1, 8);
INSERT INTO public.book_library (library_id, book_id) VALUES (2, 9);
INSERT INTO public.book_library (library_id, book_id) VALUES (2, 10);
INSERT INTO public.book_library (library_id, book_id) VALUES (2, 11);
INSERT INTO public.book_library (library_id, book_id) VALUES (2, 12);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 1);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 2);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 8);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 9);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 10);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 11);
INSERT INTO public.book_library (library_id, book_id) VALUES (3, 12);


--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 201
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_id_seq', 5, true);


--
-- TOC entry 3037 (class 0 OID 0)
-- Dependencies: 203
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.author_id_seq', 25, true);


--
-- TOC entry 3038 (class 0 OID 0)
-- Dependencies: 205
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.book_id_seq', 36, true);


--
-- TOC entry 3039 (class 0 OID 0)
-- Dependencies: 200
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 55, true);


--
-- TOC entry 3040 (class 0 OID 0)
-- Dependencies: 208
-- Name: library_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.library_id_seq', 4, true);


-- Completed on 2020-10-30 10:32:20

--
-- PostgreSQL database dump complete
--

